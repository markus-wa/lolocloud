#!/bin/zsh
start='<div class="lyrics_txt" id="lyrics_txt" style="font-size:13px; letter-spacing:0.2px; line-height:20px;">'
end='<!--'

indir=./html
outdir=./txt

mkdir -p "$outdir"

for f in $(rg -li $start $indir)
do
    infile=$(basename "$f")
    outfile=${infile%.????}.txt

    # extract lyric
    # cut div off
    # remove brs at end of line
    # remove comments
    # trim whitespace
    lyric=$(sed -n "/$start/,/$end/p" $f | \
                tail -n +2 | \
                sed 's/<br \/>//g' | \
                sed 's/<!--//g' | \
                recode html..utf8 | \
                awk '{$1=$1};1')

    echo $lyric > $outdir/$outfile
done

cat $outdir/* > merged.txt
