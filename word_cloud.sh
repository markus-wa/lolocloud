#!/bin/bash

outDir=out

lyricFile=lyrics/merged.txt
imgOutFile=$outDir/lolocloud.png
minWordLen=4
width=1448
height=312
maskImgFile=lolologo_scaled_fat.png
lolochatBgColor='#1d1d1d'
bgColor=$lolochatBgColor

mkdir -p $outDir

wordcloud_cli --text $lyricFile --imagefile $imgOutFile --min_word_length $minWordLen --width $width --height $height --mask $maskImgFile --background $bgColor
